package ytsmoviesclient.sharpmobile.ca.ytsmovieclient.view;

import android.app.ProgressDialog;
import android.app.SearchManager;
import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.Db.MovieDb;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.view.adapter.MovieAdapter;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.R;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.api.ApiClient;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.api.ApiService;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.apiresponse.ApiResult;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.apiresponse.Movie;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.MovieData;

public class MovieListActivity extends AppCompatActivity {
    private RecyclerView recyclerView ;
    private List<Movie> movies ;
    private SearchView searchView;
    private MovieAdapter movieAdapter ;
    private MovieDb movieDb;
    private List<MovieData> moviesFromDataBase;
    private List<Movie> FullData ;
    private ProgressDialog pDialog ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_movie_list);
        initViews();

        recyclerView = (RecyclerView)findViewById(R.id.recyclerview);
        LinearLayoutManager linearLayoutManager = new GridLayoutManager(getApplicationContext(),2);
        recyclerView.setLayoutManager(linearLayoutManager);

        ApiService apiService =
                ApiClient.getClient().create(ApiService.class);
        Call<ApiResult> call = apiService.getMovies();
        call.enqueue(new Callback<ApiResult>() {
            @Override
            public void onResponse(Call<ApiResult> call, Response<ApiResult> response) {
                movies = response.body().getData().getMovies() ;
                pDialog.dismiss();
                FullData = movies ;
                updateUi(movies);
            }
            @Override
            public void onFailure(Call<ApiResult> call, Throwable t) {
            }
        });
    }
    public void initViews(){
        movieDb = new MovieDb(getApplicationContext());
        pDialog = new ProgressDialog(MovieListActivity.this);
        pDialog.setMessage("Loading Data...");
        pDialog.setCancelable(true);
        pDialog.show();
    }

    public void updateUi(List<Movie> movies){
        movieAdapter = new MovieAdapter(getApplicationContext(),movies);
        recyclerView.setAdapter(movieAdapter);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search,menu);
        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        searchView = (SearchView) menu.findItem(R.id.action_search)
                .getActionView();
        searchView.setSearchableInfo(searchManager
                .getSearchableInfo(getComponentName()));
        searchView.setMaxWidth(Integer.MAX_VALUE);
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                movieAdapter.getFilter().filter(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                movieAdapter.getFilter().filter(newText);
                return false;
            }
        });

        return true ;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();
        if (id == R.id.action_search) {
            return true;
        }
        if (id ==R.id.savedmovies){
            moviesFromDataBase = movieDb.getAllUser();
            List<Movie> newMovies  = new ArrayList<>();

            for(MovieData movieData : moviesFromDataBase){
                Movie movie = new Movie();
                movie.setTitle(movieData.getTitle());
                movie.setMediumCoverImage(movieData.getCover());
                movie.setYear(movieData.getReleaseDate());
                movie.setRating(movieData.getRating());
                movie.setSummary(movieData.getSummry());
                newMovies.add(movie);
            }
            updateUi(newMovies);
        }
        if(id ==R.id.alldata){
            updateUi(FullData);
        }
        return super.onOptionsItemSelected(item);
    }

}
