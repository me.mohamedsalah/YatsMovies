package ytsmoviesclient.sharpmobile.ca.ytsmovieclient.api;

import retrofit2.Call;
import retrofit2.http.GET;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.apiresponse.ApiResult;

public interface ApiService {
    @GET("/api/v2/list_movies.json")
    Call<ApiResult> getMovies();

}
