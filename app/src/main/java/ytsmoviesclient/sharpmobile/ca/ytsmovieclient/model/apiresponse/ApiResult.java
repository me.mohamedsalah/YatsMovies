package ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.apiresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ApiResult {
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("status_message")
    @Expose
    private String statusMessage;
    @SerializedName("data")
    @Expose
    private MovieResults movieResults;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatusMessage() {
        return statusMessage;
    }

    public void setStatusMessage(String statusMessage) {
        this.statusMessage = statusMessage;
    }

    public MovieResults getData() {
        return movieResults;
    }

    public void setData(MovieResults movieResults) {
        this.movieResults = movieResults;
    }

    @Override
    public String toString() {
        return "ApiResult{" +
                "status='" + status + '\'' +
                ", statusMessage='" + statusMessage + '\'' +
                ", movieResults=" + movieResults +
                '}';
    }
}
