package ytsmoviesclient.sharpmobile.ca.ytsmovieclient.view.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import java.util.ArrayList;
import java.util.List;

import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.R;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.apiresponse.Movie;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.MovieData;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.view.MovieDetailsActivity;

public class MovieAdapter extends RecyclerView.Adapter<MovieAdapter.MovieAdapterViewHolder> implements Filterable{

    private List<Movie> movieList;
    private List<Movie> movieListFiltered;
    Context context ;
    MovieData movieData;

    public MovieAdapter(Context context,List<Movie> movieList) {
        this.movieList = movieList;
        this.context = context ;
        this.movieListFiltered = this.movieList ;
    }

    @Override
    public MovieAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.movie_item, parent, false);
        return new MovieAdapterViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MovieAdapter.MovieAdapterViewHolder holder, int position) {
        final Movie movie = movieListFiltered.get(position);

        Glide.with(context).load(movie.getMediumCoverImage())
                .placeholder(R.drawable.mediumcover)
                .into(holder.movieCover);
        holder.movieTitle.setText(" title: "+movie.getTitle());
        holder.movieCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             movieData = new MovieData(movie.getTitle(),movie.getMediumCoverImage(),
                     movie.getYear(),movie.getRating(),movie.getSummary());
                Intent intent = new Intent(v.getContext(),MovieDetailsActivity.class);
                intent.putExtra("movieDetail",movieData);
                v.getContext().startActivity(intent);
            }
        });
    }


    @Override
    public int getItemCount() {
        return movieListFiltered.size();
    }

    @Override
    public  Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if (charString.isEmpty()) {
                    movieListFiltered = movieList;
                } else {
                    List<Movie> filteredList = new ArrayList<>();
                    for (Movie row : movieList) {

                        if (row.getTitle().toLowerCase().contains(charString.toLowerCase())) {
                            filteredList.add(row);
                        }
                    }

                    movieListFiltered = filteredList;
                }

                FilterResults filterResults = new FilterResults();
                filterResults.values = movieListFiltered;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                movieListFiltered = (List<Movie>) filterResults.values;
                notifyDataSetChanged();
            }
        };

    }

    public class MovieAdapterViewHolder extends RecyclerView.ViewHolder {
        ImageView movieCover ;
        TextView movieTitle ;

        public MovieAdapterViewHolder(View itemView) {
            super(itemView);

            movieCover = itemView.findViewById(R.id.imageView);
            movieTitle = itemView.findViewById(R.id.textView);
        }


    }

}
