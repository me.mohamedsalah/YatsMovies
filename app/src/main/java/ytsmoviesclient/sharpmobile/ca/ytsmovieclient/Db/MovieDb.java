package ytsmoviesclient.sharpmobile.ca.ytsmovieclient.Db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.MovieData;

public class MovieDb extends SQLiteOpenHelper {


    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Movies.db";
    private static final String TABLE_MOVIE = "movies";

    // User Table Columns names
    private static final String COLUMN_MOVIE_ID = "movie_id";
    private static final String COLUMN_MOVIE_TITLE = "movie_title";
    private static final String COLUMN_MOVIE_RATE = "movie_rate";
    private static final String COLUMN_MOVIE_RELEASE = "movie_releaseDate";
    private static final String COLUMN_MOVIE_SUMMERY = "movie_summery";
    private static final String COLUMN_MOVIE_COVER = "movie_cover";

    // create table sql query
    private String CREATE_USER_TABLE = "CREATE TABLE IF NOT EXISTS " + TABLE_MOVIE + "("
            + COLUMN_MOVIE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + COLUMN_MOVIE_TITLE + " TEXT,"
            + COLUMN_MOVIE_RATE + " REAL," + COLUMN_MOVIE_RELEASE +
            " INTEGER,"+COLUMN_MOVIE_SUMMERY + " TEXT,"+COLUMN_MOVIE_COVER + " TEXT"+")";

    // drop table sql query
    private String DROP_USER_TABLE = "DROP TABLE IF EXISTS " + TABLE_MOVIE;

    public MovieDb(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_USER_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL(DROP_USER_TABLE);

        // Create tables again
        onCreate(db);

    }
    public void addUser(MovieData movie) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(COLUMN_MOVIE_TITLE, movie.getTitle());
        values.put(COLUMN_MOVIE_COVER, movie.getCover());
        values.put(COLUMN_MOVIE_RATE, movie.getRating());
        values.put(COLUMN_MOVIE_RELEASE, movie.getReleaseDate());
        values.put(COLUMN_MOVIE_SUMMERY, movie.getSummry());

        db.insert(TABLE_MOVIE, null, values);
        db.close();
    }

    public List<MovieData> getAllUser() {
        String[] columns = {
                COLUMN_MOVIE_TITLE,
                COLUMN_MOVIE_COVER,
                COLUMN_MOVIE_RATE,
                COLUMN_MOVIE_RELEASE,
                COLUMN_MOVIE_SUMMERY
        };

        String sortOrder =
                COLUMN_MOVIE_TITLE + " ASC";
        List<MovieData> userList = new ArrayList<MovieData>();

        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_MOVIE,
                columns,
                null,
                null,
                null,
                null,
                sortOrder);

        if (cursor.moveToFirst()) {
            do {
                MovieData movieData = new MovieData();

                movieData.setTitle(cursor.getString(cursor.getColumnIndex(COLUMN_MOVIE_TITLE)));
                movieData.setCover(cursor.getString(cursor.getColumnIndex(COLUMN_MOVIE_COVER)));
                movieData.setRating(cursor.getDouble(cursor.getColumnIndex(COLUMN_MOVIE_RATE)));
                movieData.setReleaseDate(cursor.getInt(cursor.getColumnIndex(COLUMN_MOVIE_RELEASE)));
                movieData.setSummry(cursor.getString(cursor.getColumnIndex(COLUMN_MOVIE_SUMMERY)));
                // Adding user record to list
                userList.add(movieData);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();

        return userList;
    }
}
