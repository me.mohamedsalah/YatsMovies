package ytsmoviesclient.sharpmobile.ca.ytsmovieclient.view;

import android.content.Intent;
import android.database.DatabaseUtils;
import android.databinding.DataBindingUtil;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;

import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.Db.MovieDb;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.R;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.databinding.ActivityMovieDetailsBinding;
import ytsmoviesclient.sharpmobile.ca.ytsmovieclient.model.MovieData;

public class MovieDetailsActivity extends AppCompatActivity {
    private MovieData movieData ;
    private MovieDb movieDb ;
    private ActivityMovieDetailsBinding mainBinding ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

         mainBinding = DataBindingUtil
                .setContentView(this, R.layout.activity_movie_details);
        movieDb = new MovieDb(getApplicationContext());
        Intent intent = getIntent();
        movieData = intent.getParcelableExtra("movieDetail");
        updateUi(movieData);

      }

    public void updateUi(MovieData movieData){

        Glide.with(getApplicationContext()).load(movieData.getCover())
                .into(mainBinding.imageviewD);
        mainBinding.txtTitle.setText("Title: "+movieData.getTitle());
        mainBinding.txtUr.setText("User rating: "+ movieData.getRating());
        mainBinding.txtRd.setText("Release date: "+movieData.getReleaseDate());
        mainBinding.txtSummary.setText("Plot synopsis :\n"+movieData.getSummry());
    }

 public void save(View view){ movieDb.addUser(movieData); }
}
